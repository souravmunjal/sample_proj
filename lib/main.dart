import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:physique_sample_app/core/router/app_router.gr.dart';
import 'package:uni_links/uni_links.dart';

import 'core/base_colors/colors.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(
    ProviderScope(
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          fontFamily: "Montserrat",
          primaryColor: CustomColors.orange,
          accentColor: CustomColors.orange,
          cupertinoOverrideTheme: CupertinoThemeData(
            primaryColor: CustomColors.orange,
          ),
          highlightColor: CustomColors.orange.withOpacity(0.3),
          splashColor: CustomColors.orange.withOpacity(0.3),
          textButtonTheme: TextButtonThemeData(
              style: ButtonStyle(
                  foregroundColor: MaterialStateProperty.all(Colors.black),
                  overlayColor: MaterialStateProperty.all(
                      CustomColors.orange.withOpacity(0.3)))),
          textSelectionTheme:
              TextSelectionThemeData(cursorColor: CustomColors.orange),
          inputDecorationTheme: InputDecorationTheme(
            hintStyle: TextStyle(color: CustomColors.greyDarker),
          )),
      builder: ExtendedNavigator(
        router: AppRouter(),
        initialRoute: Routes.profileScreen,
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
