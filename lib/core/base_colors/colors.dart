import 'package:flutter/material.dart';

class CustomColors {
  static Color green = Color.fromRGBO(117, 166, 140, 1);
  static Color greenII = Color.fromRGBO(235, 242, 238, 1);
  static Color greenIITransparent = Color.fromRGBO(235, 242, 238, 0.8);
  static Color disabledGrey = Color.fromRGBO(241, 241, 241, 1);
  static Color lightGrey = Color.fromRGBO(221, 221, 221, 1);
  static Color greyDarker = Color.fromRGBO(182, 182, 182, 1);
  static Color borderGrey = Color.fromRGBO(241, 238, 238, 1);
  static Color searchBorderGrey = Color.fromRGBO(241, 238, 238, 1);
  static Color mutedTextGrey = Color.fromRGBO(155, 155, 155, 1);
  static Color black = Color.fromRGBO(25, 25, 25, 1);
  static Color orange = Color.fromRGBO(243, 144, 104, 1);
  static Color appGrey = Color.fromRGBO(243, 243, 243, 1);
  static Color darkGreen = Color.fromRGBO(45, 80, 99, 1);

  static Color lightBackgroundGrey = Color.fromRGBO(250, 250, 250, 1);
  static Color lightRedI = Color.fromRGBO(255, 242, 239, 1);
  static Color lightRedII = Color.fromRGBO(255, 131, 103, 1);
  static Color errorRed = Color.fromRGBO(246, 74, 35, 1);
  static Color appRed = Color.fromRGBO(208, 33, 0, 1);
  static Color appRedLight = Color.fromRGBO(210, 30, 0, 0.05);
  static Color creamColor = Color.fromRGBO(254, 249, 234, 1);
  static Color lightGreenColor = Color.fromRGBO(70, 157, 48, 0.1);
}
