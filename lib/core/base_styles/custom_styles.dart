import 'package:flutter/material.dart';
import 'package:physique_sample_app/core/base_colors/colors.dart';

class CustomStyles {
  static TextStyle textFieldHintTextStyle =
      TextStyle(fontSize: 14, color: Colors.black, fontWeight: FontWeight.bold);

  static TextStyle extremeLargeTextStyle =
      TextStyle(fontSize: 45, color: Colors.black, fontWeight: FontWeight.bold);

  static TextStyle headerTextStyle =
      TextStyle(fontSize: 35, color: Colors.black, fontWeight: FontWeight.bold);

  static TextStyle largeBoldTextStyle =
      TextStyle(fontSize: 23, color: Colors.black, fontWeight: FontWeight.w700);
  static TextStyle style18BlackW700 =
      TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.w700);

  static TextStyle headingTextStyle =
      TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.bold);

  static TextStyle thinTextStyle =
      TextStyle(fontWeight: FontWeight.w500, color: Colors.black);

  static TextStyle normalTextStyle = TextStyle(
      fontSize: 16, color: Colors.black, fontWeight: FontWeight.normal);

  static TextStyle normal600TextStyle =
      TextStyle(fontSize: 16, color: Colors.black, fontWeight: FontWeight.w600);

  static TextStyle normal700TextStyle =
      TextStyle(fontSize: 15, color: Colors.black, fontWeight: FontWeight.w700);

  static TextStyle normal700CutTextStyle = TextStyle(
      fontSize: 15,
      color: Colors.black,
      decoration: TextDecoration.lineThrough,
      fontWeight: FontWeight.w700);
  static TextStyle normal500TextStyle =
      TextStyle(fontSize: 15, color: Colors.black, fontWeight: FontWeight.w500);
  static TextStyle normal400TextStyle =
      TextStyle(fontSize: 15, color: Colors.black, fontWeight: FontWeight.w400);
  static TextStyle normal400ClickTextTextStyle = TextStyle(
      decoration: TextDecoration.underline,
      fontSize: 15,
      color: Colors.lightBlue,
      fontWeight: FontWeight.w400);
  static TextStyle small400TextStyle =
      TextStyle(fontSize: 12, color: Colors.black, fontWeight: FontWeight.w400);
  static TextStyle small11400TextStyle =
      TextStyle(fontSize: 11, color: Colors.black, fontWeight: FontWeight.w400);

  static TextStyle mutedTextStyle = TextStyle(
      fontSize: 12,
      color: CustomColors.mutedTextGrey,
      fontWeight: FontWeight.w400);
  static TextStyle mutedTextStyle13 = TextStyle(
      fontSize: 13,
      color: CustomColors.mutedTextGrey,
      fontWeight: FontWeight.w400);
  static TextStyle mutedTextStyle10 = TextStyle(
      fontSize: 10,
      color: CustomColors.mutedTextGrey,
      fontWeight: FontWeight.w400);

  static TextStyle greenTextStyle = TextStyle(
      fontSize: 15, color: CustomColors.green, fontWeight: FontWeight.w400);
  static TextStyle redTextStyle = TextStyle(
      fontSize: 15, color: CustomColors.appRed, fontWeight: FontWeight.w400);
  static TextStyle redTextStyleExtraSmall = TextStyle(
      fontSize: 10, color: CustomColors.appRed, fontWeight: FontWeight.w400);
  static TextStyle redTextStyle13 = TextStyle(
      fontSize: 13, color: CustomColors.appRed, fontWeight: FontWeight.w500);
  static TextStyle blackTextStyle13 =
      TextStyle(fontSize: 13, color: Colors.black, fontWeight: FontWeight.w500);

  static TextStyle mutedTextStyleNormal = TextStyle(
      fontSize: 15,
      color: CustomColors.mutedTextGrey,
      fontWeight: FontWeight.w400);

  static TextStyle red700TextStyle = TextStyle(
      fontSize: 15, color: CustomColors.appRed, fontWeight: FontWeight.w700);

  static TextStyle red500TextStyle = TextStyle(
      fontSize: 15, color: CustomColors.appRed, fontWeight: FontWeight.w500);

  static TextStyle red14TextStyle = TextStyle(
      fontSize: 14, color: CustomColors.appRed, fontWeight: FontWeight.w500);

  static TextStyle black500TextStyle =
      TextStyle(fontSize: 15, color: Colors.black, fontWeight: FontWeight.w500);

  static TextStyle small600TextStyle =
      TextStyle(fontSize: 14, color: Colors.black, fontWeight: FontWeight.w600);

  static TextStyle big600TextStyle =
      TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.w600);

  static TextStyle normalBoldTextStyle =
      TextStyle(fontSize: 16, color: Colors.black, fontWeight: FontWeight.bold);

  static TextStyle smallTextStyle = TextStyle(
      fontSize: 14, color: Colors.black, fontWeight: FontWeight.normal);

  static TextStyle smallTextBoldStyle =
      TextStyle(fontSize: 14, color: Colors.black, fontWeight: FontWeight.bold);

  static TextStyle normalGreenTextStyle = TextStyle(
      fontSize: 16, color: CustomColors.green, fontWeight: FontWeight.normal);

  static TextStyle smallerTextStyle = TextStyle(
      fontSize: 12, color: Colors.black, fontWeight: FontWeight.normal);

  static TextStyle smallHeaderTextStyle =
      TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.bold);

  static TextStyle dialogTitleTextStyle =
      TextStyle(fontWeight: FontWeight.bold, fontSize: 24);

  static TextStyle dialogGreenButtonStyle =
      TextStyle(color: CustomColors.green, fontSize: 20);

  static TextStyle dialogButtonStyle =
      TextStyle(color: Colors.black, fontSize: 20);

  static TextStyle dialogButtonStyleBold =
      TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold);

  static TextStyle style20BlackNormal = TextStyle(
      color: CustomColors.black, fontSize: 20, fontWeight: FontWeight.normal);

  static TextStyle extraLargeBoldButtonStyle =
      TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold);

  static TextStyle extraLargeBoldRedButtonStyle = TextStyle(
      color: CustomColors.appRed, fontSize: 20, fontWeight: FontWeight.bold);

  static TextStyle smallNormalText =
      TextStyle(color: Colors.black, fontSize: 12, fontWeight: FontWeight.w400);
  static TextStyle smallNormalW500Text =
      TextStyle(color: Colors.black, fontSize: 12, fontWeight: FontWeight.w500);

  static TextStyle style14BlackW400 = TextStyle(
      color: CustomColors.black, fontSize: 14, fontWeight: FontWeight.w400);

  static TextStyle style14BlackNormal = TextStyle(
      color: CustomColors.black, fontSize: 14, fontWeight: FontWeight.normal);

  static TextStyle style30BlackNormal = TextStyle(
      color: CustomColors.black, fontSize: 30, fontWeight: FontWeight.normal);

  static TextStyle style55BlackNormal = TextStyle(
      color: CustomColors.black, fontSize: 55, fontWeight: FontWeight.normal);

  static TextStyle style14BlacW600 = TextStyle(
      color: CustomColors.black, fontSize: 14, fontWeight: FontWeight.w600);

  static TextStyle style25BlackW700 = TextStyle(
      color: CustomColors.black, fontSize: 25, fontWeight: FontWeight.w700);

  static TextStyle style14BOrangeW600 = TextStyle(
      color: CustomColors.orange, fontSize: 14, fontWeight: FontWeight.w600);

  static TextStyle style13BOrangeW600 = TextStyle(
      color: CustomColors.orange, fontSize: 13, fontWeight: FontWeight.w600);

  static TextStyle style14BlackUnderLineW600 = TextStyle(
    color: CustomColors.black,
    fontSize: 14,
    fontWeight: FontWeight.w600,
    decoration: TextDecoration.underline,
  );

  static InputDecoration textFieldDecoration(String hintText) {
    return InputDecoration(
        hintText: hintText,
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: CustomColors.green),
            borderRadius: BorderRadius.circular(8)),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: CustomColors.lightGrey),
            borderRadius: BorderRadius.circular(8)),
        border: OutlineInputBorder(
            borderSide: BorderSide(color: CustomColors.lightGrey),
            borderRadius: BorderRadius.circular(8)));
  }

  static Decoration greyOutlineBorder = BoxDecoration(
      border: Border.all(color: CustomColors.lightGrey),
      borderRadius: BorderRadius.circular(8));

  static Decoration whiteCardDecoration = BoxDecoration(
      color: Colors.white, borderRadius: BorderRadius.circular(10));

  static InputDecoration searchTextFieldDecoration({hintText = "Search"}) =>
      InputDecoration(
          hintText: hintText,
          contentPadding: EdgeInsets.all(0),
          fillColor: CustomColors.lightBackgroundGrey,
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(color: CustomColors.green)),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(color: CustomColors.searchBorderGrey)),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(color: CustomColors.searchBorderGrey)),
          prefixIcon: Icon(
            Icons.search,
            color: CustomColors.borderGrey,
          ));
}
