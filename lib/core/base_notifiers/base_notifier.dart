import 'dart:async';
import 'dart:io';
import 'package:auto_route/auto_route.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:physique_sample_app/core/base_models/data.dart';
import 'package:physique_sample_app/core/network/base_http_client.dart';
import 'package:physique_sample_app/core/shared_preferences/shared_preferences_data.dart';

class BaseNotifierProvider {
  static final provider =
      ChangeNotifierProvider<BaseNotifier>((ref) => new BaseNotifier());
}

class BaseNotifier extends ChangeNotifier {
  String authToken;
  bool isConnected = true;
  BaseNotifier() {
    print("Auth Notifier");
    fetchGuestToken();
    checkConnectionState();
  }

  Future<void> checkConnectionState() async {
    isConnected = await DataConnectionChecker().hasConnection;
    print("isConnection" + isConnected.toString());
    notifyListeners();

    DataConnectionChecker().onStatusChange.listen((status) async {
      if (status == DataConnectionStatus.disconnected)
        isConnected = false;
      else
        isConnected = true;
      await fetchGuestToken();
      notifyListeners();
    });
  }

  Future<void> fetchGuestToken() async {
    authToken = await SharedPreferencesData.getAuthToken();
    notifyListeners();
    bool isUserLoggedIn = await SharedPreferencesData.checkIfUserLoggedIn();
    if (authToken == null || authToken == "")
      await initOneSignalAndFetchAuthToken();
  }

  initOneSignalAndFetchAuthToken() async {
    //OneSignal Debugging
    OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);
    if (Platform.isAndroid) {
      OneSignal.shared
          .init("d267a7e2-77b6-4ab9-a94f-e82160d814fc", iOSSettings: null);
    } else {
      OneSignal.shared.init("d267a7e2-77b6-4ab9-a94f-e82160d814fc",
          iOSSettings: {
            OSiOSSettings.autoPrompt: false,
            OSiOSSettings.inAppLaunchUrl: false
          });
      // The promptForPushNotificationsWithUserResponse function will show the iOS push notification prompt.
      await OneSignal.shared
          .promptUserForPushNotificationPermission(fallbackToSettings: true);
    }
    OneSignal.shared
        .setInFocusDisplayType(OSNotificationDisplayType.notification);

    OneSignal.shared
        .setNotificationReceivedHandler((OSNotification notification) {
      print("received");
    });

    OneSignal.shared
        .setNotificationOpenedHandler((OSNotificationOpenedResult result) {
      print("clicked");
    });

    var status = await OneSignal.shared.getPermissionSubscriptionState();
    var playerId = status.subscriptionStatus.userId;
    if (playerId == null) await initOneSignalAndFetchAuthToken();
    //set the device Id
    await setDeviceInfo(playerId);
  }

  setDeviceInfo(String playerId) async {
    final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    SharedPreferencesData.setTheOneSignalId(playerId);
    if (Platform.isAndroid) {
      var build = await deviceInfoPlugin.androidInfo;
      SharedPreferencesData.setTheDeviceId(build.androidId);
      SharedPreferencesData.setTheOSType("android");
    } else {
      var build = await deviceInfoPlugin.iosInfo;
      SharedPreferencesData.setTheDeviceId(build.identifierForVendor);
      SharedPreferencesData.setTheOSType("ios");
    }
    await setGuestToken();
  }

  setGuestToken() async {
    authToken = await SharedPreferencesData.getAuthToken();
    if (authToken == "" || authToken == null) {
      String deviceId = await SharedPreferencesData.getTheDeviceId();
      String oneSignalDeviceId =
          await SharedPreferencesData.getTheOneSignalId();
      String deviceOSType = await SharedPreferencesData.getTheOSType();
      final body = {
        "device_id": deviceId,
        "device_one_signal_id": oneSignalDeviceId,
        "device_os_type": deviceOSType
      };
      final json = await BaseHttpClient.postDataWithoutAuth(
          Paths.generateGuestToken, body);
      final response = ResponseBody.fromJson(json);
      authToken = response.body['token'];
      notifyListeners();
      SharedPreferencesData.setTheAuthToken(response.body['token']);
    }
  }

  logout() async {
    final json = await BaseHttpClient.postData(Paths.logout, null);
    final response = ResponseBody.fromJson(json);
    if (response.success) {
      await SharedPreferencesData.setTheAuthToken(null);
      await SharedPreferencesData.setUserLoggedIn(false);
      await fetchGuestToken();
      // ExtendedNavigator.root.pushAndRemoveUntil(
      //   Routes.signUpScreen,
      //   (route) => false,
      // );
    }
  }
}

class Paths {
  static final String generateGuestToken = "/customer/api/v1/token/generate";
  static final String logout = "/customer/api/v1/customer/logout";
}
