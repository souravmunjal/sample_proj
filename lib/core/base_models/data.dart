class Data {
  dynamic data;
  Errors errors;
  bool isSuccess;

  Data(this.data, this.errors, this.isSuccess);
}

class ResponseBody {
  final bool success;
  final dynamic body;
  List<Errors> errors;
  final dynamic metaData;

  ResponseBody(this.success, this.body, this.errors, this.metaData);

  static ResponseBody fromJson(dynamic json) {
    if (json == null) {
      return null;
    } else if (json.isEmpty) {
      return null;
    } else {
      var response = ResponseBody(
          json['isSuccess'],
          json['body'],
          (json['errors'] != null)
              ? List.generate(json['errors'].length,
                  (index) => Errors.fromJson(json['errors'][index]))
              : null,
          json['meta']);
      return response;
    }
  }
}

class Errors {
  String code;
  String message;
  String field;

  Errors({this.code, this.message, this.field});

  Errors.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    message = json['message'];
    field = json['field'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['message'] = this.message;
    data['field'] = this.field;
    return data;
  }
}
