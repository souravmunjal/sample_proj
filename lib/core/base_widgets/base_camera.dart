import 'dart:io';

import 'package:camera/camera.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:physique_sample_app/core/base_styles/custom_styles.dart';

import 'base_scaffold.dart';
import 'file_image_item.dart';

class BaseCamera extends StatefulWidget {
  final List<String> tags = [
    'Sneaker Tag',
    'Front Photo',
    'Back Photo',
    'Dent'
  ];
  @override
  State<StatefulWidget> createState() {
    return _BaseCameraState();
  }
}

class _BaseCameraState extends State<BaseCamera> {
  CameraController controller;
  List cameras;
  int selectedCameraIdx;
  String imagePath;
  bool _isCameraLoading = true;
  double currentTagIndex = 0;
  PageController _controller = PageController(
    initialPage: 0,
  );
  double currentCameraIndex = 0;
  Map<String, String> imagesList = Map();

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      initCamera(context);
    });
    for (var i in widget.tags) imagesList[i] = "";
  }

  @override
  void dispose() {
    super.dispose();
    disposeCamera();
  }

  @override
  Widget build(BuildContext context) {
    print(imagesList);
    return BaseScaffold(
      isTitleRequired: true,
      title: "Upload Pictures",
      titleColor: Colors.black,
      isCenterTitle: true,
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: ModalProgressHUD(
          inAsyncCall: _isCameraLoading,
          dismissible: false,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                  height: 350,
                  child: PageView(
                    controller: _controller,
                    onPageChanged: (page) {
                      setState(() {
                        currentTagIndex = page.toDouble();
                      });
                    },
                    children:
                        List<Widget>.generate(widget.tags.length, (int index) {
                      return Container(
                          margin: EdgeInsets.all(20),
                          height: 350,
                          child: (imagesList[
                                      widget.tags[currentTagIndex.toInt()]] !=
                                  "")
                              ? FileImageItem(
                                  image: File(imagesList[
                                      widget.tags[currentTagIndex.toInt()]]),
                                  onImageRemove: () {
                                    setState(() {
                                      imagesList[widget
                                          .tags[currentTagIndex.toInt()]] = "";
                                    });
                                  },
                                )
                              : _cameraPreviewWidget(context));
                    }),
                  )),
              Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.fromLTRB(0, 0, 25, 25),
                  child: DotsIndicator(
                    dotsCount: widget.tags.length,
                    decorator: DotsDecorator(
                        size: Size.fromRadius(3.5),
                        activeColor: Colors.black,
                        color: Colors.grey),
                    position: currentTagIndex,
                  )),
              Text(widget.tags[currentTagIndex.toInt()].toString(),
                  style: CustomStyles.normalBoldTextStyle),
              Container(height: 11),
              Text(
                  "Take a clear picture of " +
                      widget.tags[currentTagIndex.toInt()].toString(),
                  style: CustomStyles.mutedTextStyle),
              Container(height: 28),
              GestureDetector(
                  onTap: () {
                    if (imagesList[widget.tags[currentTagIndex.toInt()]] == "")
                      _clickPictureAndCallback(context);
                  },
                  child: SvgPicture.asset('assets/images/capture_icon.svg')),
            ],
          )),
    );
  }

  void initCamera(context) {
    availableCameras().then((availableCameras) {
      cameras = availableCameras;
      if (cameras.length > 0) {
        setState(() {
          selectedCameraIdx = 0;
        });

        _initCameraController(cameras[selectedCameraIdx], context)
            .then((void v) {});
      } else {
        // widget.errorCallback("Couldn't find any cameras");
        disposeCamera();
        Navigator.of(context).pop();
        print("No camera available");
      }
    }).catchError((err) {
      // widget.errorCallback("An error occurred while starting camera");
      disposeCamera();
      Navigator.of(context).pop();
      print('Error: $err.code\nError Message: $err.message');
    });
  }

  Future _initCameraController(
      CameraDescription cameraDescription, context) async {
    setState(() {
      _isCameraLoading = true;
    });

    if (controller != null) {
      await controller.dispose();
    }
    controller = CameraController(cameraDescription, ResolutionPreset.high);

    controller.addListener(() {
      if (mounted) {
        setState(() {
          _isCameraLoading = false;
        });
      }

      if (controller.value.hasError) {
        // widget.errorCallback("An error occurred while starting camera");
        disposeCamera();
        Navigator.of(context).pop();
        print('Camera error ${controller.value.errorDescription}');
      }
    });

    try {
      await controller.initialize();
      try {
        await controller.setFlashMode(FlashMode.off);
        await controller.setFocusMode(FocusMode.auto);
      } catch (e) {
        print("couldn't set camera options");
      }
    } on CameraException catch (e) {
      // widget.errorCallback("An error occurred while starting camera");
      disposeCamera();
      Navigator.of(context).pop();
      print('Camera error ${e.toString()}');
    } catch (e) {
      // widget.errorCallback("An error occurred while starting camera");
      disposeCamera();
      Navigator.of(context).pop();
      print('Camera error ${e.toString()}');
    }

    if (mounted) {
      setState(() {
        _isCameraLoading = false;
      });
    }
  }

  void disposeCamera() async {
    await controller.dispose();
  }

  Widget _cameraPreviewWidget(context) {
    if (controller == null || !controller.value.isInitialized) {
      return Container();
    }
    final Size size = controller.value.previewSize;
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: AspectRatio(
        aspectRatio: 1,
        child: CameraPreview(controller),
      ),
    );
  }

  void _clickPictureAndCallback(context) async {
    try {
      print(controller.value.flashMode);
      final XFile file = await controller.takePicture();
      setState(() {
        imagesList[widget.tags[currentTagIndex.toInt()]] = file.path;
      });
      // widget.imagePathCallback(file.path);
      // Navigator.pop(context);
    } catch (e) {
      // widget.errorCallback("An error occurred while capturing image");
      disposeCamera();
      Navigator.of(context).pop();
      print(e);
    }
  }

  _switchCamera() {
    selectedCameraIdx =
        selectedCameraIdx < cameras.length - 1 ? selectedCameraIdx + 1 : 0;
    CameraDescription selectedCamera = cameras[selectedCameraIdx];
    _initCameraController(selectedCamera, context);
  }
}
