import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:physique_sample_app/core/base_notifiers/base_notifier.dart';

class BaseScaffold extends ConsumerWidget {
  final String title;
  final bool isCenterTitle;
  final Widget body;
  final bool resizeToAvoidBottomInset;
  final Function() back;
  final List<Widget> actionMenu;
  final bool isTitleRequired;
  final Color titleColor;
  final Color backgroundColor;

  BaseScaffold(
      {this.title,
      this.isCenterTitle,
      this.body,
      this.resizeToAvoidBottomInset,
      this.back,
      this.actionMenu,
      this.isTitleRequired,
      this.titleColor,
      this.backgroundColor});

  @override
  Widget build(BuildContext context,
      T Function<T>(ProviderBase<Object, T> provider) watch) {
    print("base scaffold");
    var baseProvider = watch(BaseNotifierProvider.provider);
    var authToken = baseProvider.authToken;
    bool isConnected = baseProvider.isConnected;

    return Scaffold(
      backgroundColor: backgroundColor,
      resizeToAvoidBottomInset: resizeToAvoidBottomInset,
      body: SafeArea(
          child: (isConnected)
              ? (authToken == "" || authToken == null)
                  ? ModalProgressHUD(
                      inAsyncCall: true, dismissible: false, child: body)
                  : ModalProgressHUD(inAsyncCall: false, child: body)
              : Center(child: Text("please check your internet connection"))),
      appBar: (isTitleRequired == true)
          ? AppBar(
              elevation: 0,
              brightness: Brightness.light,
              backgroundColor: Colors.white,
              centerTitle: isCenterTitle,
              title: Text(
                title,
                style: TextStyle(color: titleColor, fontSize: 16),
              ),
              leading: IconButton(
                icon: Icon(
                  Icons.arrow_back_ios_rounded,
                  color: Colors.black,
                ),
                onPressed: () =>
                    (back == null) ? Navigator.of(context).pop() : back(),
              ),
              actions: actionMenu,
            )
          : null,
    );
  }
}
