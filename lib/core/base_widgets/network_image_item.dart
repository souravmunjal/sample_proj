import 'package:flutter/cupertino.dart';

class NetworkImageItem extends StatelessWidget {
  final String imageUrl;
  final double radius;

  NetworkImageItem(this.imageUrl, {Key key, this.radius = 10})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: ClipRRect(
            borderRadius: BorderRadius.circular(radius),
            child: FadeInImage(
                fit: BoxFit.cover,
                image: NetworkImage(
                  imageUrl,
                ),
                placeholder:
                    AssetImage("assets/images/placeholder_grey_default.png"))));
  }
}
