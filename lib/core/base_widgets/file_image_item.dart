import 'dart:io';

import 'package:flutter/material.dart';

class FileImageItem extends StatelessWidget {
  final File image;
  final VoidCallback onImageRemove;

  const FileImageItem({Key key, this.image, this.onImageRemove})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
            margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
            child: Stack(children: [
              Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: FileImage(image, scale: 0.3),
                          fit: BoxFit.cover),
                      borderRadius: BorderRadius.circular(10))),
              Container(
                padding: EdgeInsets.all(5),
                child: InkWell(
                  onTap: onImageRemove,
                  child: Container(
                    height: 20,
                    width: 20,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color.fromRGBO(0, 0, 0, 0.3)),
                    child: Icon(
                      Icons.clear,
                      size: 12,
                      color: Colors.white,
                    ),
                  ),
                ),
                alignment: Alignment.topRight,
              ),
            ])));
  }
}
