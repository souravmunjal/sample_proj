import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:physique_sample_app/core/base_colors/colors.dart';

class DarkRedColorButton extends StatelessWidget {
  final String text;
  final Function onPressed;

  DarkRedColorButton({@required this.text, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return TextButton(
        child: Text(
          text,
          style: TextStyle(fontSize: 13),
        ),
        onPressed: onPressed,
        style: ButtonStyle(
            foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
            backgroundColor:
                MaterialStateProperty.all<Color>(CustomColors.appRed),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                    side: BorderSide(color: CustomColors.appRed)))));
  }
}

class GreyColorButton extends StatelessWidget {
  final String text;
  final Function onPressed;

  GreyColorButton({@required this.text, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return TextButton(
        child: Text(
          text,
          style: TextStyle(fontSize: 13),
        ),
        onPressed: onPressed,
        style: ButtonStyle(
            foregroundColor:
                MaterialStateProperty.all<Color>(CustomColors.darkGreen),
            backgroundColor:
                MaterialStateProperty.all<Color>(CustomColors.appGrey),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                    side: BorderSide(color: CustomColors.appGrey)))));
  }
}

class RedBorderButton extends StatelessWidget {
  final String text;
  final Function onPressed;

  RedBorderButton({@required this.text, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: Text(
        text,
        style: TextStyle(fontSize: 13, color: CustomColors.appRed),
      ),
      onPressed: onPressed,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
          side: BorderSide(color: CustomColors.appRed)),
    );
  }
}

class LightColoredButton extends StatelessWidget {
  final String text;
  final Function onPressed;

  LightColoredButton({this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return TextButton(
        child: Text(text, style: TextStyle(fontSize: 13)),
        onPressed: onPressed,
        style: ButtonStyle(
            foregroundColor: MaterialStateProperty.all<Color>(Colors.black),
            backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                    side: BorderSide(color: Colors.black)))));
  }
}

class DarkColoredButton extends StatelessWidget {
  final String text;
  final Function onPressed;
  final Widget widget;

  DarkColoredButton(
      {@required this.text, @required this.onPressed, this.widget});

  @override
  Widget build(BuildContext context) {
    return TextButton(
        child: Text(
          text,
          style: TextStyle(fontSize: 13),
        ),
        onPressed: onPressed,
        style: ButtonStyle(
            foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
            backgroundColor: MaterialStateProperty.all<Color>(Colors.black),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)))));
  }
}

class DarkColoredButtonWithIcon extends StatelessWidget {
  final String text;
  final Function onPressed;
  final Widget widget;

  DarkColoredButtonWithIcon(
      {@required this.text, @required this.onPressed, @required this.widget});

  @override
  Widget build(BuildContext context) {
    return TextButton(
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              widget,
              Container(
                width: 10,
              ),
              Text(
                text,
                style: TextStyle(fontSize: 13),
              )
            ]),
        onPressed: onPressed,
        style: ButtonStyle(
            foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
            backgroundColor: MaterialStateProperty.all<Color>(Colors.black),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)))));
  }
}

class TransparentBorderedButton extends StatelessWidget {
  final String text;
  final Function onPressed;

  TransparentBorderedButton({this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return TextButton(
        child: Text(text, style: TextStyle(fontSize: 13)),
        onPressed: onPressed,
        style: ButtonStyle(
            foregroundColor: MaterialStateProperty.all<Color>(Colors.black),
            backgroundColor:
                MaterialStateProperty.all<Color>(Colors.transparent),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                    side: BorderSide(color: Colors.black)))));
  }
}

class DisableButton extends StatelessWidget {
  final String text;

  DisableButton({this.text});

  @override
  Widget build(BuildContext context) {
    return TextButton(
        child: Text(text, style: TextStyle(fontSize: 13)),
        onPressed: () {},
        style: ButtonStyle(
            foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
            backgroundColor:
                MaterialStateProperty.all<Color>(CustomColors.greyDarker),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                    side: BorderSide(color: CustomColors.greyDarker)))));
  }
}
