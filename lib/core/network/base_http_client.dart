import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';
import 'package:physique_sample_app/core/shared_preferences/shared_preferences_data.dart';

class BaseHttpClient {
  static Client client;
  static String baseURL = "s-mainstreet.styfi.in";
  static Client getClient() {
    if (client == null) {
      client = new Client();
    }
    return client;
  }

  static Future<dynamic> fetchData(String path, {var queryParameter}) async {
    String token = await SharedPreferencesData.getAuthToken();
    Uri url = Uri.https(baseURL, path, queryParameter);
    print("token" + token);
    final response = await getClient()
        .get(url, headers: {"authorization": "Bearer " + token});
    return json.decode(response.body);
  }

  static Future<dynamic> postImage(
      String path, String filePath, String fileName, String customerId,
      {var queryParameter}) async {
    String token = await SharedPreferencesData.getAuthToken();
    Uri url = Uri.https(baseURL, path, queryParameter);
    var request = MultipartRequest(
      'POST',
      url,
    );
    request.files.add(await MultipartFile.fromPath("image", filePath,
        contentType: MediaType('image', 'jpeg')));
    request.headers.addAll({
      "authorization": "Bearer " + token,
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    request.fields.addAll({
      'title': fileName,
      'url': 'customers/profile-picture',
      "foreign_id": customerId,
      'foreign_type': 'customers'
    });
    var response = await request.send();
    final respStr = await response.stream.bytesToString();

    // FormData formData = new FormData.fromMap({
    //   'title': fileName,
    //   'url': 'customers/profile-picture',
    //   "foreign_id": customerId,
    //   'foreign_type': 'customers',
    //   "file1": UploadFileInfo(new File("./upload.jpg"), "upload1.jpg")
    // });

    return json.decode(respStr);
  }

  static Future<dynamic> postData(String path, var body,
      {var queryParameter}) async {
    String token = await SharedPreferencesData.getAuthToken();
    Uri url = Uri.https(baseURL, path, queryParameter);
    print("token" + token);
    final response = await getClient().post(url, body: body, headers: {
      'Content-type': 'application/json',
      "authorization": "Bearer " + token
    });
    return json.decode(response.body);
  }

  static Future<dynamic> patchApi(String path, var body,
      {var queryParameter}) async {
    String token = await SharedPreferencesData.getAuthToken();
    Uri url = Uri.https(baseURL, path, queryParameter);
    print(body.toString() + "bodyddddd");
    print("Bearer " + token);
    final response = await getClient().patch(url, body: body, headers: {
      'Content-type': 'application/json',
      "authorization": "Bearer " + token
    });

    return json.decode(response.body);
  }

  static Future<dynamic> postDataWithoutAuth(String path, var body,
      {var queryParameter}) async {
    Uri url = Uri.https(baseURL, path, queryParameter);
    final response = await getClient().post(url, body: body);
    print(response);
    return json.decode(response.body);
  }
}
