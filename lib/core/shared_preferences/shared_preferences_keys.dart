class SharedPreferencesKeys {
  static const String IS_USER_LOGGED_IN = "is_user_logged_in";
  static const String AUTH_TOKEN = "auth_token";
  static const String DEVICE_ID = "device_id"; //  DEVICE_ID id type String
  static const String OS_TYPE = "os_type"; // OS_TYPE type String String
  static const String ONE_SIGNAL_ID =
      "one_signal_id"; //ONE_SIGNAL_ID type String
}
