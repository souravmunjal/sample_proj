import 'package:shared_preferences/shared_preferences.dart';

import 'shared_preferences_keys.dart';

class SharedPreferencesData {
  static Future<SharedPreferences> getSharedPreferences() async {
    return await SharedPreferences.getInstance();
  }

  static dynamic checkIfUserLoggedIn() async {
    SharedPreferences sharedPreferences = await getSharedPreferences();
    return sharedPreferences.get(SharedPreferencesKeys.IS_USER_LOGGED_IN);
  }

  static void setUserLoggedIn(bool isLoggedIn) async {
    SharedPreferences sharedPreferences = await getSharedPreferences();
    sharedPreferences.setBool(
        SharedPreferencesKeys.IS_USER_LOGGED_IN, isLoggedIn);
  }

  static dynamic getAuthToken() async {
    SharedPreferences sharedPreferences = await getSharedPreferences();
    return sharedPreferences.get(SharedPreferencesKeys.AUTH_TOKEN);
  }

  static void setTheAuthToken(String token) async {
    SharedPreferences sharedPreferences = await getSharedPreferences();
    sharedPreferences.setString(SharedPreferencesKeys.AUTH_TOKEN, token);
  }

  static dynamic getTheOSType() async {
    SharedPreferences sharedPreferences = await getSharedPreferences();
    return sharedPreferences.get(SharedPreferencesKeys.OS_TYPE);
  }

  static void setTheOSType(String osType) async {
    SharedPreferences sharedPreferences = await getSharedPreferences();
    sharedPreferences.setString(SharedPreferencesKeys.OS_TYPE, osType);
  }

  static dynamic getTheDeviceId() async {
    SharedPreferences sharedPreferences = await getSharedPreferences();
    return sharedPreferences.get(SharedPreferencesKeys.DEVICE_ID);
  }

  static void setTheDeviceId(String deviceId) async {
    SharedPreferences sharedPreferences = await getSharedPreferences();
    sharedPreferences.setString(SharedPreferencesKeys.DEVICE_ID, deviceId);
  }

  static dynamic getTheOneSignalId() async {
    SharedPreferences sharedPreferences = await getSharedPreferences();
    return sharedPreferences.get(SharedPreferencesKeys.ONE_SIGNAL_ID);
  }

  static void setTheOneSignalId(String oneSignalId) async {
    SharedPreferences sharedPreferences = await getSharedPreferences();
    sharedPreferences.setString(
        SharedPreferencesKeys.ONE_SIGNAL_ID, oneSignalId);
  }
}
