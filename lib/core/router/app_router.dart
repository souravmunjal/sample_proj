import 'package:auto_route/auto_route_annotations.dart';
import 'package:physique_sample_app/features/profile/presentation/screens/profile_screen.dart';

@AdaptiveAutoRouter(
  routes: <AutoRoute>[AutoRoute(page: ProfileScreen)],
)
class $AppRouter {}
