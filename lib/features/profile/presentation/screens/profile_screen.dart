import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_animation_progress_bar/flutter_animation_progress_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';

//Colors
final Color appGrey = Color.fromRGBO(243, 243, 243, 1);
final Color darkGreen = Color.fromRGBO(45, 80, 99, 1);
final Color disabledGrey = Color.fromRGBO(241, 241, 241, 1);
final Color borderGrey = Color.fromRGBO(241, 238, 238, 1);
final Color black = Color.fromRGBO(25, 25, 25, 1);
final Color orange = Color.fromRGBO(243, 144, 104, 1);

//Text Styles
final TextStyle style14BlackNormal = TextStyle(
    color: black,
    fontSize: 14,
    fontWeight: FontWeight.normal,
    fontFamily: "Montserrat");

final TextStyle style14BOrangeW600 = TextStyle(
    color: orange,
    fontSize: 14,
    fontWeight: FontWeight.w600,
    fontFamily: "Montserrat");

final TextStyle style25BlackW700 = TextStyle(
    color: black,
    fontSize: 25,
    fontWeight: FontWeight.w700,
    fontFamily: "Montserrat");

final TextStyle style13BOrangeW600 = TextStyle(
    color: orange,
    fontSize: 13,
    fontWeight: FontWeight.w600,
    fontFamily: "Montserrat");

final TextStyle style18BlackW700 = TextStyle(
    color: black,
    fontSize: 18,
    fontWeight: FontWeight.w700,
    fontFamily: "Montserrat");

class ProfileScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ProfileScreenState();
  }
}

class _ProfileScreenState extends State<ProfileScreen> {
  var progressVal = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          brightness: Brightness.light,
          backgroundColor: Colors.white,
          title: Text(""),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_rounded,
              color: Colors.black,
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        body: Container(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: RefreshIndicator(
                onRefresh: () {},
                child: ListView(
                  children: [
                    Container(
                      width: 140,
                      height: 140,
                      child: Image.asset("assets/images/profile_image.png"),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                      ),
                    ),
                    Container(
                      width: 35,
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 35, 0, 20),
                      height: 100,
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Container(
                            height: 100,
                            width: 100,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              image: DecorationImage(
                                image: AssetImage("assets/images/mandala.png"),
                                fit: BoxFit.fitWidth,
                                alignment: Alignment.center,
                              ),
                            ),
                          ),
                          Container(
                            width: 20,
                          ),
                          Flexible(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Level 5",
                                  style: style14BOrangeW600,
                                ),
                                Container(
                                  height: 5,
                                ),
                                Text(
                                  "Sourav",
                                  style: style25BlackW700,
                                ),
                                Container(
                                  height: 15,
                                ),
                                Stack(children: [
                                  FAProgressBar(
                                    backgroundColor: disabledGrey,
                                    progressColor: darkGreen,
                                    size: 20,
                                    borderRadius: 12,
                                    animatedDuration:
                                        Duration(seconds: 1, milliseconds: 500),
                                    currentValue: progressVal,
                                    maxValue: 1500,
                                  ),
                                  Container(
                                      margin:
                                          EdgeInsets.only(left: 15, top: 2.5),
                                      child: Text(
                                        "1200/1500 XP",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 12,
                                        ),
                                      ))
                                ]),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          top: 30, left: 20, right: 20, bottom: 30),
                      child: Divider(
                        thickness: 1,
                        color: borderGrey,
                      ),
                    ),
                    Row(
                      children: [
                        SvgPicture.asset("assets/images/insta_icon.svg"),
                        Container(
                          width: 10,
                        ),
                        Text(
                          "Connect Your Instagram",
                          style: style14BlackNormal,
                        ),
                        Spacer(),
                        Text(
                          "Connect",
                          style: style13BOrangeW600,
                        ),
                      ],
                    ),
                    Container(
                      height: 50,
                    ),
                    Text(
                      "General",
                      style: style18BlackW700,
                    ),
                    Container(
                      height: 27,
                    ),
                    Row(
                      children: [
                        SvgPicture.asset(
                            "assets/images/personal_info_icon.svg"),
                        Container(
                          width: 10,
                        ),
                        Text(
                          "Personal information",
                          style: style14BlackNormal,
                        ),
                      ],
                    ),
                    Container(
                      height: 15,
                    ),
                    Divider(
                      thickness: 1,
                      color: borderGrey,
                    ),
                    Container(
                      height: 15,
                    ),
                    Row(
                      children: [
                        SvgPicture.asset("assets/images/heart_icon.svg"),
                        Container(
                          width: 10,
                        ),
                        Text(
                          "Favourites",
                          style: style14BlackNormal,
                        ),
                      ],
                    ),
                    Container(
                      height: 50,
                      margin: EdgeInsets.only(
                          left: 0, right: 0, top: 60, bottom: 20),
                      width: double.infinity,
                      child: TextButton(
                          child: Text(
                            "Log out",
                            style: TextStyle(fontSize: 13),
                          ),
                          onPressed: () {},
                          style: ButtonStyle(
                              foregroundColor:
                                  MaterialStateProperty.all<Color>(darkGreen),
                              backgroundColor:
                                  MaterialStateProperty.all<Color>(appGrey),
                              shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8),
                                      side: BorderSide(color: appGrey))))),
                    ),
                  ],
                ))));
  }
}
