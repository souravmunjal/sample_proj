import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:physique_sample_app/core/base_models/data.dart';
import 'package:physique_sample_app/features/profile/data/model/profile_data.dart';
import 'package:physique_sample_app/features/profile/data/repository/profile_repository.dart';

class ProfileNotifier extends StateNotifier<AsyncValue<ProfileData>> {
  final ProfileRepository profileRepository;
  ProfileNotifier(this.profileRepository) : super(AsyncValue.loading()) {
    fetchUserDetails();
  }

  Future<void> fetchUserDetails() async {
    try {
      var profileData = await profileRepository.fetchProfileData();
      if (profileData is List<Errors>)
        state = AsyncError(profileData);
      else
        state = AsyncData(profileData);
    } catch (e) {
      List<Errors> errors = [];
      errors
          .add(Errors(code: "xx", message: e.toString(), field: e.toString()));
      state = AsyncError(errors);
    }
  }
}
