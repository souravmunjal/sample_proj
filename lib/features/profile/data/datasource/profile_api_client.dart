import 'package:flutter/cupertino.dart';
import 'package:physique_sample_app/core/base_models/data.dart';
import 'package:physique_sample_app/features/profile/data/model/profile_data.dart';

class ProfileApiClient {
  Future<ResponseBody> fetchProfileData() async {
    await Future.delayed(Duration(seconds: 5));
    return ResponseBody(true, ProfileData("Sourav", "7", "1200"), null, null);
  }
}
