import 'package:physique_sample_app/core/base_models/data.dart';
import 'package:physique_sample_app/features/profile/data/datasource/profile_api_client.dart';

class ProfileRepository {
  final ProfileApiClient profileApiClient;

  ProfileRepository(this.profileApiClient);

  Future<dynamic> fetchProfileData() async {
    ResponseBody responseBody = await profileApiClient.fetchProfileData();
    if (responseBody.success) return responseBody.body;
    return responseBody.errors;
  }
}
