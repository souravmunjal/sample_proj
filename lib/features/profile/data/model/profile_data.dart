class ProfileData {
  final String firstName;
  final String level;
  final String points;

  ProfileData(this.firstName, this.level, this.points);
}
